**IDE:** ![Visual Studio Code](https://img.shields.io/badge/-Visual%20Studio%20Code-blue?style=flat-square&logo=visual-studio-code)\
**Browser:** ![Brave](https://img.shields.io/static/v1?style=flat-square&logo=Brave&label=&message=Brave&color=white) ![Tor Browser](https://img.shields.io/static/v1?style=flat-square&logo=Tor%20Browser&label=&message=Tor%20Browser&color=blueviolet)\
**Apps from:** ![F-Droid](https://img.shields.io/static/v1?style=flat-square&logo=F-Droid&label=&message=F-Droid&color=black)\
**OS:** ![Manjaro](https://img.shields.io/static/v1?style=flat-square&logo=Manjaro&label=&message=Manjaro&color=black)

# Tech stack
![JavaScript](https://img.shields.io/badge/-JavaScript-black?style=flat-square&logo=javascript&label=Frontend)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=orange&label=Frontend)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3&label=Frontend)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-563D7C?style=flat-square&logo=bootstrap&label=Frontend)
![TypeScript](https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=typescript&label=Frontend)
![MongoDB](https://img.shields.io/badge/-MongoDB-black?style=flat-square&logo=mongodb&label=Database)
![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-336791?style=flat-square&logo=postgresql&label=Database)
![Docker](https://img.shields.io/badge/-Docker-black?style=flat-square&logo=docker&label=Container)
![Git](https://img.shields.io/badge/-Git-black?style=flat-square&logo=git&label=Version%20Control)
![GitHub](https://img.shields.io/badge/-GitHub-181717?style=flat-square&logo=github&label=Version%20Control)
![GitLab](https://img.shields.io/badge/-GitLab-FCA121?style=flat-square&logo=gitlab&label=Version%20Control)
![Laravel](https://img.shields.io/static/v1?style=flat-square&logo=Laravel&label=&message=Laravel&color=white)
![Composer](https://img.shields.io/static/v1?style=flat-square&logo=Composer&message=Composer&color=brown&labelColor=black&label=Package%20manager)
![CodePen](https://img.shields.io/static/v1?style=flat-square&logo=CodePen&label=&message=CodePen&color=black)
![CoffeeScript](https://img.shields.io/static/v1?style=flat-square&logo=CoffeeScript&label=Frontend&message=CoffeeScript&color=black)
![Cytoscape.js](https://img.shields.io/static/v1?style=flat-square&logo=Cytoscape.js&label=Frontend&message=Cytoscape.js&color=black)
![FontAwesome](https://img.shields.io/static/v1?style=flat-square&logo=FontAwesome&label=Icon%20Font&message=FontAwesome&color=blue)
![Genius](https://img.shields.io/static/v1?style=flat-square&logo=Genius&label=Community&message=Genius&color=yellow)
![JSON](https://img.shields.io/static/v1?style=flat-square&logo=JSON&label=Data%20format&message=JSON&color=black)
![Lua](https://img.shields.io/static/v1?style=flat-square&logo=Lua&label=Server-side&message=Lua&color=blueviolet)
![Ruby](https://img.shields.io/static/v1?style=flat-square&logo=Ruby&label=Server-side&message=Ruby&color=red)
![MySQL](https://img.shields.io/static/v1?style=flat-square&logo=MySQL&label=&message=MySQL&color=white)
![neo4j](https://img.shields.io/static/v1?style=flat-square&logo=neo4j&label=Database&message=neo4j&color=black)
![nginx](https://img.shields.io/static/v1?style=flat-square&logo=nginx&label=Server&message=nginx&color=green&logoColor=green)
![sentry](https://img.shields.io/static/v1?style=flat-square&logo=sentry&message=sentry&color=black&label=Monitoring)
![SQLite](https://img.shields.io/static/v1?style=flat-square&logo=SQLite&label=Database&message=SQLite&color=black&logoColor=blue)
![Stack Overflow](https://img.shields.io/static/v1?style=flat-square&logo=Stack%20Overflow&label=Community&message=Stack%20Overflow&color=black)
![Telegram](https://img.shields.io/static/v1?style=flat-square&logo=Telegram&label=Messenger&message=Telegram&color=black)
![Apache](https://img.shields.io/badge/Tools-Apache-informational?style=flat-square&logo=apache&logoColor=white&color=D22128&label=Server)
![Insomnia](https://img.shields.io/badge/Tools-Insomnia-informational?style=flat-square&logo=insomnia&logoColor=white&color=5849BE&label=API)


[![shaedrich's github stats](https://github-readme-stats.vercel.app/api?username=shaedrich&show_icons=true&count_private=true)](https://github.com/anuraghazra/github-readme-stats)
[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=shaedrich)](https://github.com/anuraghazra/github-readme-stats)

